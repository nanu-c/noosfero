#get docker
apt update
apt install thin git
apt-get install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
apt update
apt-get install docker-ce
#get docker-compose
curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

#set docker network
docker network create --driver bridge reverse-proxy

#set nginx proxy
docker run -d -p 80:80 -p 443:443 \
    --name nginx-proxy \
    --net reverse-proxy \
    -v $HOME/certs:/etc/nginx/certs:ro \
    -v /etc/nginx/vhost.d \
    -v /usr/share/nginx/html \
    -v /var/run/docker.sock:/tmp/docker.sock:ro \
    --label com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy=true \
    jwilder/nginx-proxy
#set letsencrypt
docker run -d \
        --name nginx-letsencrypt \
        --net reverse-proxy \
        --volumes-from nginx-proxy \
        -v $HOME/certs:/etc/nginx/certs:rw \
        -v /var/run/docker.sock:/var/run/docker.sock:ro \
        jrcs/letsencrypt-nginx-proxy-companion

#get the noosfero
git clone noosfero
cd noosfero

#generate noosfero configuration
thin -C config/thin.yml -e production config
# and configure it
nano config/thin.yml
# copy it
mkdir $HOME/config
cp config/thin.yml $HOME/config/thin.yml
docker-compose -f roadmap.yml build
docker-compose -f roadmap.yml up -d
docker network connect noosfero_back nginx-proxy

#todo in ruby console

#Setting the Default Environment domain

d = Domain.new( :name => "roadmap.convive.io", :is_default => true )
Environment.default.domains << d
Environment.default.save

#Set a admin user
p = Person['batman']
Environment.default.add_admin p
